$(inicializar);


function inicializar(){
  if (('#frmPortada').length>0)  leer_datos_seccion();
  if (('#frmServicios').length>0) leer_datos_servicios();
  $('#frmPortada').find('input[name=cbSeccion]').change(leer_datos_seccion);
  $('#frmServicios').find('input[name=cbServicio]').change(leer_datos_servicios);
}

function leer_datos_seccion(){
  var data_param={ 
        func: 'leer_datos_seccion',
        cbSeccion: $("input[name='cbSeccion']:checked").val()
      }
  console.log(data_param);
  $.post(url_admin_ajax, data_param, function(data) {
    console.log(data);
    $('#txtTitulo').val(data.portada.titulo);
    $('#txtTexto').val(data.portada.texto);
    imagen=base_uploads+'/portada/'+data.portada.imagen;
    $('#imagen_portada').attr('src',imagen),
    inicializar_file_input(imagen);
  });
}


function leer_datos_servicios(){
  var data_param={ 
        func: 'leer_datos_servicios',
        cbServicio: $("input[name='cbServicio']:checked").val()
      }
  console.log(data_param);
  $.post(url_admin_ajax, data_param, function(data) {
    console.log(data);
    $('#txtTitulo').val(data.servicio.titulo);
    $('#txtParrafo1').val(data.servicio.parrafo1);
    $('#txtParrafo2').val(data.servicio.parrafo2);
    imagen=base_uploads+'/servicios/'+data.servicio.imagen;
    $('#imagen_servicio').attr('src',imagen),
    inicializar_file_input(imagen);
  });
}

  function inicializar_file_input(imagen){  
    $("#file-es").fileinput({
          showUpload: false,
          showCaption: false,
          remove:false,
          browseClass: "btn btn-primary",
          fileType: "any",
          previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
          overwriteInitial: true,
          initialPreviewAsData: true,
         /* initialPreview: [
              imagen
          ],
          initialPreviewConfig: [
              {caption: "transport-1.jpg", size: 329892, width: "120px", url: "{$url}", key: 1}
          ]*/
      });

  }


