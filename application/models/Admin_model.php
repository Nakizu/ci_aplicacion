<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}


	public function guardar_portada($ar,$arFile){
		$ar['imagen'] = $arFile["file"]["name"];
		if ($ar['imagen'] && copy($arFile['file']['tmp_name'], './uploads/img/portada/'.$ar['imagen'])){
			$this->db->set('imagen', $ar['imagen']);	
		}
		$this->db->where('seccion', $ar['cbSeccion']);
		$q=$this->db->get('portada');
		$this->db->set('titulo', $ar['titulo']);
		$this->db->set('texto', $ar['texto']);
		$this->db->set('seccion', $ar['cbSeccion']);
		if ($q->num_rows()>0){
			$r=$q->row_array();
			$this->db->where('id', $r['id']);
			$this->db->update('portada');
		}else{
			$this->db->insert('portada');
		}
	}

	public function guardar_servicios($ar,$arFile){
		//echo '<pre>';print_r($ar);print_r($arFile);die();
		$ar['imagen'] = $arFile["file"]["name"];
		echo $ar['imagen'].'<br>';echo $arFile['file']['tmp_name'];
		if ($ar['imagen'] && copy($arFile['file']['tmp_name'], './uploads/img/servicios/'.$ar['imagen'])){
			$this->db->set('imagen', $ar['imagen']);	
		}
		$this->db->where('servicio', $ar['cbServicio']);
		$q=$this->db->get('servicios');
		$this->db->set('titulo', $ar['titulo']);
		$this->db->set('parrafo1', $ar['parrafo1']);
		$this->db->set('parrafo2', $ar['parrafo2']);
		$this->db->set('servicio', $ar['cbServicio']);
		if ($q->num_rows()>0){
			$r=$q->row_array();
			$this->db->where('id', $r['id']);
			$this->db->update('servicios');
		}else{
			$this->db->insert('servicios');
		}	
	}


	public function leer_datos_portada($ar=false){
		if (!$ar){
			return $this->db->get('portada')->result_array();	
		}else{
			$this->db->where('seccion', $ar['cbSeccion']);
			return $this->db->get('portada')->row_array();	
		}
		
	}


}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */