<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}


	public function alta_usuario($ar){
		$res=array();
		$res['class']='aviso-error';
		$ar['clave_encriptada']=$this->encriptar_clave($ar['clave']);
		if ($this->no_existe_email($ar)){
			$this->db->set('fecha', 'NOW()', false);
			$this->db->set('registro_ip', $this->input->ip_address());
			$this->db->set('email', $ar['email']);
			$this->db->set('clave', $ar['clave_encriptada']);
			$this->db->insert('usuarios');
			$res['class']='aviso-ok';
			$res['mensaje']=lang('registro.registrado');
		}else{
			$res['mensaje']=lang('registro.email_existe');
		}
		return $res;
	}

	public function no_existe_email($ar){
		$this->db->where('email', $ar['email']);
		$q=$this->db->get('usuarios');
		return $q->num_rows()==0;
	}

	public function encriptar_clave($clave){
		$clave_codificada=md5($clave);
		return $clave_codificada;
	}

	public function comprobar_usuario($ar){
		$ar['clave_encriptada']=$this->encriptar_clave($ar['clave']);
		$this->db->where('email', $ar['email']);
		$this->db->where('clave', $ar['clave_encriptada']);
		$q=$this->db->get('usuarios');
		if ($q->num_rows()==0){
			$res['class']='aviso-error';
			$res['mensaje']=lang('registro.no_login');
			return $res;
		}else{
			redirect('admin');
		}
	}
}

/* End of file Users_model.php */
/* Location: ./application/models/Users_model.php */