<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios_model extends CI_Model {


	
	public function leer_datos_servicios($ar=false){
		if (!$ar){
			return $this->db->get('servicios')->result_array();	
		}else{
			$this->db->where('servicio', $ar['cbServicio']);
			return $this->db->get('servicios')->row_array();	
		}
		
	}	


}

/* End of file Servicios_model.php */
/* Location: ./application/models/Servicios_model.php */