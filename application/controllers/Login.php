<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	var $data=array();

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users_model','users');
		$this->lang->load('mensajes');
		$this->data['msj']=array();
	}

	public function index()
	{	
	}


	public function login($tipo=false){
		$arPost = array();
		while(list($key, $val)=each($_POST)) $arPost[$key] = $this->input->post($key);
		
		switch($this->input->post('accion')){
			case 'entrar':
				$this->data['msj']=$this->users->comprobar_usuario($arPost);
				break;
			case 'registrar':
				$this->data['msj']=$this->users->alta_usuario($arPost);
				break;
			case 'olvidar':
				break;				
		}
		switch ($tipo) {
			case 'registro':				
					if (isset($this->data['msj']['class']) && $this->data['msj']['class']=='aviso-ok')
						$vista_contenido='login/registro_ok';
					else
				        $vista_contenido='login/registro';
				break;
			case 'olvido':
				$vista_contenido='login/olvido';

				break;
			default:
				$vista_contenido='login/login';
				break;
		}
		
		$this->data['cont']=$this->load->view($vista_contenido, $this->data, TRUE);
				
		$this->load->view('login/layout', $this->data, FALSE);

	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */