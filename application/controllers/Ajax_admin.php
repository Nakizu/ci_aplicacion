<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_admin extends CI_Controller {

	var $data=array();

	public function __construct()
	{
		parent::__construct();
		header('Content-Type: application/json');
		$this->data['ok'] = 0;
		$this->data['error'] = 0;
		$this->load->model('Admin_model','admin');
		$this->load->model('Servicios_model','servicios');
	}
	public function index()
	{
		
	}


   public function app_post(){
        $data['ok'] = 0;
        $arPost  = array();
        while (list($key, $val) = each($_POST)) {
            $arPost[$key] = $this->input->post($key);
        }

        switch ($this->input->post('func')) {
        	case 'leer_datos_seccion':
        		$this->data['portada']=$this->admin->leer_datos_portada($arPost);
        		$this->data['ok']=1;
    		break;
            case 'leer_datos_servicios':
                $this->data['servicio']=$this->servicios->leer_datos_servicios($arPost);
                $this->data['ok']=1;
            break;
    	default:
    		break;
    	}
    	echo json_encode($this->data);
        exit();
    }
}

/* End of file Ajax_admin.php */
/* Location: ./application/controllers/Ajax_admin.php */