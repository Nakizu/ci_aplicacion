<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends My_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Servicios_model','servicios');
	}
	public function index(){
		$this->data['servicios']=$this->servicios->leer_datos_servicios();
		$this->cargar_vista('servicios');
	}

}

/* End of file Servicios.php */
/* Location: ./application/controllers/Servicios.php */