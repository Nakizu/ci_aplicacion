<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactar extends My_Controller {

	public function index()
	{

 		$data['crumbs'][]  = array('texto' => lang('menu.inicio'), 'url' => '');
        $data['crumbs'][]  = array('texto' => lang('menu.contactar'), 'url' => '#');
        
        $data['tituloa']    = lang('menu.contactar_titulo');
        $data['subtituloa'] = lang('menu.contactar_subtitulo');
        $menuSel='contactar';
      
		$this->cargar_vista('contactar');
	}

}

/* End of file Contactar.php */
/* Location: ./application/controllers/Contactar.php */