<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends My_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model','admin');
		$this->lang->load('mensajes');
		
		//Do your magic here
	}

	public function index(){

	}

	public function portada(){
		$arPost = array();
		while(list($key, $val)=each($_POST)) $arPost[$key] = $this->input->post($key);
		switch($this->input->post('accion')){
			case 'guardar':
					$this->admin->guardar_portada($arPost,$_FILES);
				break;
		}
		$this->data['title']='tituloa';
		$this->data['mensajes_sin_leer']=array();
		$this->data['notificaciones_sin_leer']=array();
		$this->data['menuSel']='inicio';
		$this->cargar_vista_admin('inicio');
	}

	public function servicios(){
		$arPost = array();
		while(list($key, $val)=each($_POST)) $arPost[$key] = $this->input->post($key);
		switch($this->input->post('accion')){
			case 'guardar':
					$this->admin->guardar_servicios($arPost,$_FILES);
				break;
		}
		$this->data['title']='tituloa';
		$this->data['mensajes_sin_leer']=array();
		$this->data['notificaciones_sin_leer']=array();
		$this->data['menuSel']='servicios';
		$this->cargar_vista_admin('servicios');
	}
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */