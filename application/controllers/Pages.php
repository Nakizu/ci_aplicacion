<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends My_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
		
		
	}

	function view($page='home'){
		$data=array();
        $data['title'] = ucfirst($page); // Capitalize the first letter
        $template='templates/modern-business';
        $this->load->view($template.'/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view($template.'/footer', $data);
	}


	
	public function idioma($lang){
		$this->session->set_userdata(array('idioma'=>$lang));				
		redirect($_SERVER['HTTP_REFERER']);
	}

}

/* End of file Pages.php */
/* Location: ./application/controllers/Pages.php */