<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imagenes extends My_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Imagenes_model','imagenes');
		//Do your magic here
	}
	public function index()
	{
		$this->data['res']=$this->imagenes->leer_imagenes();
		$this->cargar_vista('imagenes3');
		//$this->load->view('aplicacion/pages/imagenes', $this->data, FALSE);
	}

}

/* End of file Imagenes.php */
/* Location: ./application/controllers/Imagenes.php */