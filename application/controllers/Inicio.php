<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends My_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model','admin');
		//Do your magic here
	}

	public function index()
	{	$this->data['title']='tituloa';
		$this->data['portada']=$this->admin->leer_datos_portada();
		$this->cargar_vista('inicio');
	}

	

}

/* End of file Inicio.php */
/* Location: ./application/controllers/Inicio.php */