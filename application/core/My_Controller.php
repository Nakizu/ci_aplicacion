<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {

	var $data=array();

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('mensajes');
		$this->data['crumbs'][]  = array();	        
	}


	function cargar_vista($vista,$vista_simple=false){
		$this->data['title']='G93 Telecomunicaciones';
		$template='aplicacion/templates/'.PLANTILLA;
		if ($vista_simple){
			$this->load->view('aplicacion/'.$vista, $this->data);
		}else{
			$this->load->view($template.'/header', $this->data);
			$this->data['vista_titulo_breadcrumbs']=$this->load->view($template.'/titulo_bread_crumbs', $this->data,TRUE);
	        $this->load->view('aplicacion/pages/'.$vista, $this->data);
	        $this->load->view($template.'/footer', $this->data);	
		}
	}

	function cargar_vista_admin($vista,$vista_simple=false){
		$this->data['title']='G93 Telecomunicaciones';
		$template='admin/templates/'.PLANTILLA_ADMIN;
		if ($vista_simple){
			$this->load->view('admin/'.$vista, $this->data);
		}else{
			$this->load->view($template.'/header', $this->data);
			$this->data['vista_titulo_breadcrumbs']=$this->load->view($template.'/titulo_bread_crumbs', $this->data,TRUE);
	        $this->load->view('admin/pages/'.$vista, $this->data);
	        $this->load->view($template.'/footer', $this->data);	
		}
	}



}

/* End of file My_Controller.php */
/* Location: ./application/controllers/My_Controller.php */