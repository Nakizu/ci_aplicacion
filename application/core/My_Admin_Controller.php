<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Admincontroller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('mensajes');
		$crumbs[]  = array();	        
	}

function cargar_vista($vista,$data,$vista_simple=false){
		$data['title']='G93 Telecomunicaciones';
		$template='admin/templates/'.PLANTILLA_ADMIN;
		if ($vista_simple){
			$this->load->view('admin/'.$vista, $data);
		}else{
			$this->load->view($template.'/header', $data);
			$data['vista_titulo_breadcrumbs']=$this->load->view($template.'/titulo_bread_crumbs', $data,TRUE);
	        $this->load->view('admin/pages/'.$vista, $data);
	        $this->load->view($template.'/footer', $data);	
		}
	}




}

/* End of file My_Controller.php */
/* Location: ./application/controllers/My_Controller.php */