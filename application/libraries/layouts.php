<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layouts
{
	protected $ci;

	private $layout_title=null;
	private $layout_description=null;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function set_title($title){
		$this->layout_title=$title;
	}

	public function set_description($description){
		$this->layout_description=$description;
	}

	public function view($view_name, $layouts = array(), $params, $default=TRUE){
		if (is_array($layouts) && count($layouts)>0){
			foreach ($layouts as $key => $layout) {
				$params[$key]=$this->ci->$this->load->view($layout $params, TRUE);
			}
		}
			
		if ($default){
			$params['layout_title']=$this->layout_title;
			$params['layout_description']=$this->layout_description;
			//render default header
			$this->ci->load->view('layouts/header',$params);
			//render content
			$this->ci->load->view($view_name,$params);
			//render default footer
			$this->ci->load->view('layouts/footer',$params);
		}else{
			$this->ci->load->view($view_name, $params, FALSE);	
		}
	}

	

}

/* End of file layouts.php */
/* Location: ./application/libraries/layouts.php */
