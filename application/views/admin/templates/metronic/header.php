<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>LINKINSIDERS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="LINKINSIDERS" name="description" />
        <meta content="www.linkinsiders.com" name="author" />


        <meta name="keywords" content="" />
        <meta name="description" content="Es un lugar de encuentro para profesionales que colaboran entre si para encontrar nuevos clientes utilizando los contactos comerciales que ya tienen." />    
        <meta property="og:title" content="LINKINSIDERS" />
        <meta property="og:description" content="Es un lugar de encuentro para profesionales que colaboran entre si para encontrar nuevos clientes utilizando los contactos comerciales que ya tienen." />
        <meta property="og:image" content="" />



        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>assets/metronic/global/plugins/icheck-1.x/skins/line/orange.css" rel="stylesheet">

		<link href="<?php echo base_url(); ?>assets/bootstrap-fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/bootstrap-fileinput-master/themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>
    

<?php /* ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/third/datatables/datatables.min.css">
<?php */ ?>

        <!--link href="<?php echo base_url(); ?>assets/<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" /-->

<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        
        
        <!--link href="<?php echo base_url(); ?>assets/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" /-->

        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />

<?php //para las alertas ?>
<link href="<?php echo base_url(); ?>/assets/metronic/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url(); ?>/assets/metronic/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />



<!--link href="<?php echo base_url(); ?>/assets/v1/tooltipster-master/dist/css/tooltipster.bundle.min.css" rel="stylesheet" type="text/css" /-->


        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url(); ?>assets/metronic/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/metronic/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url(); ?>assets/metronic/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/metronic/layouts/layout3/css/themes/yellow-orange.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url(); ?>assets/metronic/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/metronic/pages/css/profile.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/metronic/global/plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css" />

        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />

        <link href="<?php echo base_url(); ?>assets/app/css/layout.css?rand=<?php echo date('YmdHis'); ?>" rel="stylesheet" type="text/css" />


<!-- Start of linkinsiders Zendesk Widget script -->

<!-- End of linkinsiders Zendesk Widget script -->





</head>
    <!-- END HEAD -->


    <body class="page-container-bg-solid page-header-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header navbar navbar-fixed-top">
                        <!-- BEGIN HEADER TOP -->
                        <div class="page-header-top">
                            <div class="container-fluid">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                    <a href="#"><img src="<?php echo base_url('assets/img/logo-g93.png');?>" alt="logo" class="logo-default"></a>
                                </div>
                                <!-- END LOGO -->
                                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                <a href="javascript:;" class="menu-toggler"></a>
                                <!-- END RESPONSIVE MENU TOGGLER -->
                                <!-- BEGIN TOP NAVIGATION MENU -->
                                <div class="top-menu">
                                    <ul class="nav navbar-nav pull-right">
                                        <!-- BEGIN INBOX DROPDOWN -->
                                        <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">
                                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                <span class="circle"><?php echo count($mensajes_sin_leer); ?></span>
                                                <span class="corner"></span>
                                            </a>
                                            <?php /* ?>
                                            <ul class="dropdown-menu">
                                                <li class="external">
                                                    <h3><?php echo lang('mensajes.tienes').'<strong>'. count($mensajes_sin_leer). lang('mensajes'); ?></h3>
                                                    <a href="<?php echo site_url('app/emails');?>"><?php echo lang('mensajes.ver_todos'); ?></a>
                                                </li>
                                                <li>
                                                    <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                                        <?php foreach ($mensajes_sin_leer as $mensaje) {?>
                                                        <li>
                                                            <a href="<?php echo site_url('app/emails/editar').'/'.codificar_base64($mensaje['id']);?>">
                                                                <span class="photo">
                                                                    <!--img src="<?php echo base_url(); ?>assets/metronic/layouts/layout3/img/avatar.png" class="img-circle" alt=""-->

                                                                    <img  class="item-pic rounded" src="<?php echo base_url('assets/ap/img/avatars/avatar'.$mensaje['num_avatar'].'.png')?>" alt="avatar">
                                                                </span>
                                                                
                                                                <span class="subject">
                                                                    <span class="from"><?php echo $mensaje['alias']; ?></span>
                                                                    <span class="time"><?php echo diferencia_tiempo($mensaje['fecha'],$this->lang->lang()); ?></span>
                                                                </span>
                                                                <span class="message"><?php echo $mensaje['asunto']; ?></span>

                                                            </a>
                                                        </li>
                                                        <?php }?>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <?php */ ?>
                                        </li>
                                        <!-- END INBOX DROPDOWN -->
                                        <!-- BEGIN USER LOGIN DROPDOWN -->
<!--MENU USUARIO-->
                                        <li class="dropdown dropdown-user dropdown-dark">
                                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                            
                                            <img id="imagen_avatar" class="item-pic rounded" src="<?php echo base_url('assets/ap/img/avatars/avatar2.png')?>"  alt="avatar">

                                            <span class="username username-hide-mobile"><?php echo 'alias' ?></span>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-default">
                                                <li><a href="<?php echo site_url('app/suscripcion'); ?>"><i class="icon-arrow-up"></i> Pasar a PRO</a></li>
                                                <li class="divider"> </li>
                                                <li><a href="<?php echo site_url('app/perfil'); ?>"><i class="icon-user"></i> <?php echo lang('menu.mis_datos'); ?> </a></li>
                                                <li><a href="<?php echo site_url('app/links'); ?>"><i class="icon-link"></i> <?php echo lang('menu.mis_links'); ?> <span class="badge badge-danger" id="total_links"></span></a></li>
                                                <li><a href="<?php echo site_url('app/emails'); ?>"><i class="icon-envelope-open"></i><?php echo lang('menu.mensajes'); ?> <span class="badge badge-danger"> <?php echo count($mensajes_sin_leer); ?> </span></a></li>
                                                <li><a href="<?php echo site_url('app/notificaciones'); ?>"><i class="icon-bell"></i><?php echo lang('menu.avisos'); ?><span class="badge badge-success"><?php echo count($notificaciones_sin_leer); ?></span></a></li>
                                                <?php /*?>
                                                <li><a href="#"><i class="icon-eye"></i> <?php echo lang('menu.privacidad'); ?></a></li>
                                                <li><a href="#"><i class="icon-settings"></i> <?php echo lang('menu.preferencias'); ?></a></li>
                                                <?php */?>
                                                <li class="divider"> </li>
                                                <li><a href="<?php echo site_url('app/perfil/salir'); ?>"><i class="icon-key"></i> <?php echo lang('menu.cerrar_sesion'); ?></a></li>
                                            </ul>
                                        </li>
                                        <!-- END USER LOGIN DROPDOWN -->
                                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                                       <?php /* ?>
                                        <li class="dropdown dropdown-extended quick-sidebar-toggler dropdown-dark">
                                            <span class="sr-only">Toggle Quick Sidebar</span>
                                            <i class="icon-logout"></i>
                                        </li>
                                        <?php */ ?>
                                        <!-- END QUICK SIDEBAR TOGGLER -->
                                    </ul>
                                </div>
                                <!-- END TOP NAVIGATION MENU -->
                            </div>
                        </div>
                        <!-- END HEADER TOP -->
                        <!-- BEGIN HEADER MENU -->
                        <div class="page-header-menu">
                            <div class="container-fluid">
                                <!-- BEGIN HEADER SEARCH BOX -->
                                <?php /* ?>
                                <form class="search-form" action="index.php?sec=buscadorempresas" method="GET">
                                    <div class="input-group">
                                       <input type="hidden" name="sec" value="buscadorempresas" />
                                        <input class="form-control" placeholder="Search" name="query" type="text">
                                        <span class="input-group-btn"><a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a></span>
                                    </div>
                                </form>
                                <?php */ ?>
                                <!-- END HEADER SEARCH BOX -->
                                <!-- BEGIN MEGA MENU -->
                                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                                <div class="hor-menu">
                                    <ul class="nav navbar-nav">
<?php //MENU INICIO ?>
                                       <li aria-haspopup="true"<?php if ($menuSel == "inicio") {echo ' class="active"';} ?>>
                                        <a href="<?php echo site_url('admin'); ?>" class="nav-link">
                                            <i class="icon-home"></i><?php echo lang('menu.inicio'); ?> 
                                        </a></li>
<?php //MENU MENU1 ?>
                                        <li aria-haspopup="true"  class="<?php if ($menuSel == "servicios") { echo ' active';}?>">
                                            <a href="<?php echo site_url('admin/servicios'); ?>" class="nav-link">

                                            <i class="icon-share"></i>  
                                            <?php echo lang('menu.servicios');?>
                                         </a></li>
<?php //MENU MENU2 ?>
                                    <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if ($menuSel == "menu2") { echo ' active';}?>">
                                        <a href="javascript:;"> <i class="fa fa-building-o" aria-hidden="true"></i>
                                        <?php echo lang('menu.menu2'); ?>  <span class="arrow"></span></a>
                                        <ul class="dropdown-menu pull-left" style="min-width: 300px">
                                            
                                            <li aria-haspopup="true"><a href="<?php echo site_url('menu2/menu21'); ?>" class="nav-link<?php if ($menuSel == "buscadorempresas") {echo ' active';}?>"><i class="icon-magnifier"></i> <?php echo lang('menu.empresas.buscador'); ?></a></li>
                                            
                                            <li aria-haspopup="true"><a href="<?php echo site_url('menu2/menu21');?>" class="nav-link"><i class="icon-plus"></i> <?php echo lang('menu.empresas.add_contacto'); ?></a></li>

                                            <li aria-haspopup="true">
                                                <a href="<?php echo site_url('menu2/menu23'); ?>" class="nav-link"><i class="icon-user"></i> 
                                                <?php echo lang('menu.empresas.donde_tengo_contactos'); ?>
                                                </a>
                                            </li>

                                            <li aria-haspopup="true"><a href="<?php echo site_url('app/contactos/busco'); ?>"  class="nav-link"><i class="icon-magnifier"></i> <?php echo lang('menu.empresas.donde_busco_contactos');?></a></li>
                                        </ul>
                                    </li>
                                   
<?php //MENU MENU3 ?>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if ($menuSel == "menu3") { echo ' active';}?>">
                                            <a href="javascript:;"><i class="icon-user"></i> <?php echo lang('menu.menu3'); ?> <span class="arrow"></span></a>
                                            <ul class="dropdown-menu pull-left" style="min-width: 300px">
                                                <li aria-haspopup="true"><a href="<?php echo site_url('menu3/menu31/'); ?>" class="nav-link"><i class="icon-user"></i> <?php echo lang('menu.menu31'); ?></a></li>

                                                <li aria-haspopup="true"><a href="<?php echo site_url('menu3/menu32'); ?>" class="nav-link<?php if ($menuSel == "menu3") { echo ' active';}?>">
                                                <i class="icon-magnifier"></i> <?php echo lang('menu.menu33');?></a></li>

                                                
                                            </ul>
                                        </li>
<!--MENU MEJORAR SUSCRIPCION-->
                                        <!--li aria-haspopup="true" class="linkinsider"><a href="#" class="nav-link<?php if ($menuSel == "insiders") {
    echo ' active';
}
?>"><i class="icon-rocket"></i> Mejorar suscripción</a></li-->
                                    </ul>
                                </div>
                                <!-- END MEGA MENU -->
                            </div>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!-- END HEADER -->
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">

<?php //TITULO Y SUBTITULO ?>

                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container-fluid">
                                    <!-- BEGIN PAGE TITLE -->
                                    <div class="page-title">
                                        <h1> <span id="titulo_layout"><?php if (isset($tituloa)) echo $tituloa; ?></span> <small><span id="subtitulo_layout"><?php if (isset($subtituloa)) echo $subtituloa; ?></span></small></h1>
                                    </div>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
<?php //BREAD CRUMBS ?>
                            <div class="page-content">
                                <div class="container-fluid">
                                <div class="col-md-12">
                                    <!-- BEGIN PAGE BREADCRUMBS -->
                                    <ul class="page-breadcrumb breadcrumb">
                                    <?php /* if (!empty($crumbs)) {?>
                                        <?php foreach ($crumbs as $crumb) {?>
                                        <li>
                                            <?php if ($crumb == end($crumbs)) {echo '<span>' . $crumb['texto'] . '</span>';} else {?>
                                            <a href="<?php echo site_url($crumb['url']);?>"><?php echo $crumb['texto']; ?></a>
                                            <i class="fa fa-circle"></i>
                                            <?php }?>
                                        </li>
                                        <?php }}?>
                                        <li id="total_resultados" class="pull-right">
                                        <?php if (isset($res['total_resultados']) && $res['total_resultados'] > 0) {
                                            echo 'Total: ' . number_format($res['total_resultados'],0,',','.');
                                        }*/?>
                                    </li>
                                    </ul>
                                    </div>
                                    <!-- END PAGE BREADCRUMBS -->

                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">
                                    <?php if (isset($vistaContenido)) {
                                            echo $vistaContenido;
                                    }?>
                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
