
<!-- Page Heading/Breadcrumbs -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> <?php if (isset($tituloa)) echo $tituloa ?>
            <small><?php if (isset($subtituloa)) echo $subtituloa ?></small>
        </h1>
        <ol class="breadcrumb">
            <?php if (count($crumbs) > 1) {?>
                <?php foreach ($crumbs as $crumb) {?>
                    <li>
                        <?php 
                             if ($crumb == end($crumbs)) {echo '<span>' . $crumb['texto'] . '</span>';} else {?>
                            <a href="<?php echo site_url($crumb['url']);?>"><?php echo $crumb['texto']; ?></a>
                            <i class="fa fa-circle"></i>
                        <?php }?>
                    </li>
                    
                <?php }}?>
        </ol>
    </div>
</div>
<!-- /.row -->
