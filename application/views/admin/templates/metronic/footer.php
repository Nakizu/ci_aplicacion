    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner text-center">
             2017 &copy; G93 Telecomunicaciones
        </div>
        
    </div>
    <!-- END FOOTER -->
   
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/respond.min.js"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/excanvas.min.js"></script> 
    <![endif]-->
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <!--script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery.cokie.min.js" type="text/javascript"></script-->
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <script src="<?php  echo base_url('');?>assets/bootstrap-fileinput-master/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/bootstrap-fileinput-master/js/fileinput.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/bootstrap-fileinput-master/js/locales/es.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/bootstrap-fileinput-master/themes/explorer/theme.js" type="text/javascript"></script>
    

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <script src="<?php  echo base_url('');?>assets/metronic/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
    <!--script src="<?php  echo base_url('');?>assets/metronic/global/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script-->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php  echo base_url('');?>assets/metronic/layouts/layout/scripts/layout.js"></script>
    <script src="<?php echo base_url(); ?>assets/metronic/global/plugins/icheck-1.x/icheck.js"></script>
    <script src="<?php echo base_url(); ?>assets/metronic/global/plugins/dropzone/dropzone.min.js"></script>
     <script type="text/javascript">
        var url_admin = '<?php echo site_url('admin'); ?>';
        var url_admin_ajax = '<?php echo site_url('ajax_admin/app_post'); ?>';
        var base_uploads = '<?php echo base_url('uploads/img');?>'
    </script>

    <script src="<?php echo base_url(); ?>assets/admin/js/core.js"></script>

    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
    
    jQuery(document).ready(function() {    
       App.init(); // init metronic core componets
       Layout.init(); // init layout
       QuickSidebar.init() // init quick sidebar
       Index.init();   
       Index.initDashboardDaterange();
       Index.initJQVMAP(); // init index page's custom scripts
       Index.initCalendar(); // init index page's custom scripts
       Index.initCharts(); // init index page's custom scripts
       Index.initChat();
       Index.initMiniCharts();
       Index.initIntro();
       Tasks.initDashboardWidget();
    });
    </script>
    <!-- END JAVASCRIPTS -->
     </body>

    </html>