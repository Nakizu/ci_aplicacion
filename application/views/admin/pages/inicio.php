
<div class="mt-content-body">
<form action="<?php echo current_url(); ?>" class="dropzone" id="frmPortada" method="post" enctype="multipart/form-data" onSubmit="return validar_registro()">

<input type="hidden" name="accion" id="funcion" value="guardar" />
                	    <!-- Content Row -->
    <div class="row">
        <div class="col-md-2">
        	<div class="portlet light" style="min-height:400px">
        		<div class="portlet-title">
        		
        		</div>
				<div class="portlet-body">

					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbSeccion" id="cbSeccion" value="1" checked>
					    Sección 1
					  </label>
					</div>
					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbSeccion" id="cbSeccion" value="2">
					    Sección 2
					  </label>
					</div>
					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbSeccion" id="cbSeccion" value="3">
					    Sección 3
					  </label>
					</div>
					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbSeccion" id="cbSeccion" value="4">
					    Sección 4
					  </label>
					</div>
					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbSeccion" id="cbSeccion" value="5">
					    Sección 5
					  </label>
					</div>


				</div>
			</div>
        </div>

		<div class="col-md-10">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption caption-md">
						<h4 class="caption-subject font-yellow-gold bold uppercase"><?php echo lang('admin.seccion');?></h4>
					</div>
				</div><!--div class="portlet-title"-->

				<div class="portlet-body">
					<div class="form-group">
            			<label><?php echo lang('admin.titulo'); ?></label>
            			<input type="text" name="titulo" id="txtTitulo" class="form-control" value="<?php if (isset($portada['titulo'])) echo $portada['titulo'];else echo '';?>" required />
            		</div>

            		<div class="form-group">
            			<label><?php echo lang('admin.texto'); ?></label>
            			<textarea name="texto" id="txtTexto" class="form-control" required rows="5"><?php if (isset($portada['texto'])) echo $portada['texto'];?></textarea>
            		</div>

            		
					<div class="form-group">
            			<label><?php echo lang('admin.imagen'); ?></label>
            			<img src="" alt="imagen" id="imagen_portada" style="height:200px; width:auto">
            		</div>

					<div class="form-group">
				    	<input id="file-es" name="file" type="file">
					</div>
					<div class="form-group">
						<div class="text-right">
							<button type="submit" class="btn blue"><?php echo lang('guardar') ?></button>
						</div>	
					</div>
				</div><!--div class="portlet-body"-->

			</div>
        </div>
     </div>
</form>
 </div>