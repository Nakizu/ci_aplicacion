
<div class="mt-content-body">
<form action="<?php echo current_url(); ?>" id="frmServicios" method="post" enctype="multipart/form-data" onSubmit="return validar_registro()">

<input type="hidden" name="accion" id="funcion" value="guardar" />
                	    <!-- Content Row -->
    <div class="row">
        <div class="col-md-2">
        	<div class="portlet light" style="min-height:400px">
        		<div class="portlet-title">
        		
        		</div>
				<div class="portlet-body">

					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbServicio"  value="1" checked>
					    Servicio 1
					  </label>
					</div>
					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbServicio"  value="2">
					    Servicio 2
					  </label>
					</div>
					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbServicio"  value="3">
					    Servicio 3
					  </label>
					</div>
					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbServicio"  value="4">
					    Servicio 4
					  </label>
					</div>
					<div class="form-check">
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="cbServicio"  value="5">
					    Servicio 5
					  </label>
					</div>


				</div>
			</div>
        </div>

		<div class="col-md-10">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption caption-md">
						<h4 class="caption-subject font-yellow-gold bold uppercase"><?php echo lang('admin.servicios');?></h4>
					</div>
				</div><!--div class="portlet-title"-->

				<div class="portlet-body">
					<div class="form-group">
            			<label><?php echo lang('admin.titulo'); ?></label>
            			<input type="text" name="titulo" id="txtTitulo" class="form-control" value="<?php if (isset($servicio['titulo'])) echo $servicio['titulo'];else echo '';?>" required />
            		</div>
            		
            		<div class="form-group">
            			<label><?php echo lang('admin.parrafo1'); ?></label>
            			<textarea name="parrafo1" id="txtParrafo1" class="form-control" required rows="5"><?php if (isset($servicio['parrafo1'])) echo $servicio['parrafo1'];?></textarea>
            		</div>
					
					<div class="form-group">
            			<label><?php echo lang('admin.parrafo2'); ?></label>
            			<textarea name="parrafo2" id="txtParrafo2" class="form-control" required rows="5"><?php if (isset($servicio['parrafo2'])) echo $servicio['parrafo2'];?></textarea>
            		</div>
					
					<div class="form-group">
            			<label><?php echo lang('admin.imagen'); ?></label>
            			<img src="" alt="imagen" id="imagen_servicio" style="height:200px; width:auto">
            		</div>

					<div class="form-group">
				    	<input id="file-es" name="file" type="file">
					</div>
					<div class="form-group">
						<div class="text-right">
							<button type="submit" class="btn blue"><?php echo lang('guardar') ?></button>
						</div>	
					</div>
				</div><!--div class="portlet-body"-->

			</div>
        </div>
     </div>
</form>
 </div>