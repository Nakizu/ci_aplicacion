      <!-- Footer -->
        <footer>
            <div id="footer_plantilla"  class="row">
                <div  class="col-lg-12 text-center">
                    <h3>Copyright &copy; 2017</h3>
                </div> 
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url('assets/startbootstrap-modern-business');?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/startbootstrap-modern-business');?>/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
