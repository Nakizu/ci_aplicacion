<?php //echo '<pre>';print_r($servicios); ?>
<div class="container">
<div class="mt-content-body">
    <!-- Service Tabs -->
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header"><?php echo lang('servicios.titulo')?></h2>
        </div>
        <div class="col-lg-12">

            <ul id="myTab" class="nav nav-pills nav-fill">
            <?php 
                $i=0;
                foreach ($servicios as $servicio) { ?>
                   <li class="<?php if ($i==0) echo 'active';?>"><a href="#service_<?php echo $i;?>" data-toggle="tab"><i class="fa fa-tree"></i> <?php echo $servicio['titulo'];?></a>
                    </li>            

            <?php $i++;} ?>
            </ul>

            <div id="myTabContent" class="tab-content">
            <?php 
                $i=0;
                foreach ($servicios as $servicio) { ?>
                    <div class="tab-pane fade <?php if ($i==0) echo 'active in';?>" id="service_<?php echo $i;?>">
                        <h4><?php echo $servicio['titulo'];?></h4>
                        <p><?php echo $servicio['parrafo1'];?></p>
                        <p><?php echo $servicio['parrafo2'];?></p>
                        <img class="img-responsive" src="<?php echo base_url('uploads/img/servicios').'/'.$servicio['imagen'];?>" alt="imagen"/>

                   </div>
                

            <?php $i++; }?>

            </div>

        </div>
    </div>
</div>
</div>