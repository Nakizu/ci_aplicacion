    <!-- Page Content -->
    <div class="container">

        <?php if (isset($vista_titulo_breadcrumbs)) echo $vista_titulo_breadcrumbs; ?>

        <!-- Content Row -->
        <div class="row">
            <!-- Map Column -->
            <div class="col-md-8">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2904.5236838698825!2d-2.1701191850606985!3d43.28235368435045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd51c84658f2b671%3A0x630fe487bb29590!2sZelai+Ondo+Enparantza+Plaza%2C+12%2C+20800+Zarautz%2C+Gipuzkoa!5e0!3m2!1ses!2ses!4v1502433798853" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>  
            </div>
            <!-- Contact Details Column -->
            <div class="col-md-4">
                <h3>Nerea Akizu</h3>
                <p>
                    Zelai Ondo <br>Zarautz<br>
                </p>
                <p><i class="fa fa-phone"></i> 
                    <abbr title="Phone">P</abbr>: (943) 010101</p>
                <p><i class="fa fa-envelope-o"></i> 
                    <abbr title="Email">E</abbr>: <a href="mailto:nakizu77@gmail.com">nakizu77@gmail.com</a>
                </p>
                <p><i class="fa fa-clock-o"></i> 
                    <abbr title="Hours">H</abbr>: Lunes - Viernes: 9:00 a 15:00</p>
                <ul class="list-unstyled list-inline list-social-icons">
                    <li>
                        <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-linkedin-square fa-2x"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.row -->

        <!-- Contact Form -->
        <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
        <div class="row">
            <div class="col-lg-8 pull-center">
                <h3><?php echo lang('contactar.titulo_envio');?></h3>
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo lang('nombre');?></label>
                            <input type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                            <p class="help-block"></p>
                        </div>
                    </div>                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo lang('email');?></label>
                            <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo lang('mensaje');?></label>
                            <textarea rows="10" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
                        </div>
                    </div>
                    <div id="success"></div>
                    <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-primary"><?php echo lang('enviar');?></button>
                </form>
            </div>

        </div>
        <!-- /.row -->
