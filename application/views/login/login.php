<section id="acceso" class="accesosection">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
               	<div class="text-center" id="acceso-header">
					<h2 id="headerlogo"><span>G93</span>Telecomunicaciones</h2>
					<h1><?php echo lang('registro.inicia_sesion'); ?></h1>
					<p>O <a href="<?php echo site_url('login/registro');?>" ><?php echo lang('registro.registrate'); ?></a> <?php echo lang('registro.cuenta'); ?> </p>
				</div>
               	
               <div id="mensaje_aviso" class="<?php if (empty($msj)) echo 'hidden';?>">
                    <div class="aviso <?php if (isset($msj['class'])) echo $msj['class']?>"><?php if (isset($msj['mensaje'])) echo $msj['mensaje']?></div>
                </div>


               	<!--div class="aviso aviso-error">Este es el mensaje de que algo ha fallado y ejemplo de <a href="#">enlace dentro</a></div-->
            
               
                <div class="formulario">
                	<form action="<?php echo current_url(); ?>" method="post" onSubmit="return validar_login()">
                        <input type="hidden" name="accion" value="entrar" />
                		<div class="form-group">
                			<label><?php echo lang('registro.correo'); ?></label>
                			<input type="email" name="email" id="txtEmail" class="form-control" value="<?php if (isset($arDatos['email'])) echo $arDatos['email'];else echo '';?>" required />
                		</div>
                		<div class="form-group">
                			<label><?php echo lang('registro.contrasena'); ?></label>
                			<input type="password" name="clave" class="form-control" value="<?php if (isset($arDatos['clave'])) echo $arDatos['clave'];else echo '';?>"  required/>
                		</div>
                        
                        <div class="form-group">
                        <?php //echo $this->recaptcha->imprimir(); ?>
                        </div>
                        
                		<div class="text-center">
                            <button type="submit"><?php echo lang('registro.iniciar_sesion'); ?></button>
                        </div>
                	</form>
                </div>
                <div class="footer-nav">
                	<ul>
						<li><a href="<?php echo site_url('login/registro');?>"><?php echo lang('registro.registrate'); ?></a></li>
           				<li><a href="<?php echo site_url('login/olvido');?>"><?php echo lang('registro.olvido'); ?></a></li>
					</ul>
				</div>
            </div>
        </div>
    </div>
</section>
