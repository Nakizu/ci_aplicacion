<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>G93 Telecomunicaciones</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" />
	<!-- Bootstrap Core CSS -->
	<link href="<?php echo base_url(); ?>assets/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/app/css/login.css?rand=<?php echo rand(); ?>" rel="stylesheet" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="toplink"><a href="<?php echo site_url()?>"><?php echo lang('registro.volver_web');?></a></div>

    <?php if (isset($cont)) echo $cont; ?>


    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/metronic/global/plugins/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/metronic/global/plugins/bootstrap/js/bootstrap.min.js"></script>


<!-- Piwik -->
<script type="text/javascript">
/*  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//stats.linkinsiders.com/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '<?php echo PWIK_ID; ?>']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();*/
</script>
<!-- End Piwik Code -->

    <script type="text/javascript">
        var url_app_ajax = '<?php echo site_url('ajax_login/app'); ?>';
        var base_assets = '<?php echo base_url('assets'); ?>';
    </script>


    <script src='https://www.google.com/recaptcha/api.js'></script> 
    <script src="<?php echo base_url(); ?>assets/ap/js/login.js?rand=<?php echo rand(); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/ap/js/mensajes/<?php echo $this->lang->lang(); ?>.js?rand=<?php echo date('YmdHis'); ?>"></script>

</body>

</html>
