 <section id="acceso" class="accesosection">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="text-center" id="acceso-header">
                        <h2 id="headerlogo"><span>G93</span>Telecomunicaciones</h2>
                        <h1><?php echo lang('registro.olvido'); ?></h1>
                        <p><?php echo lang('registro.olvido_explicacion'); ?></p>
                    </div>

                    <div id="mensaje_aviso" class="<?php if (empty($msj)) echo 'hidden';?>">
                        <div class="aviso <?php if (isset($msj['class'])) echo $msj['class']?>"><?php if (isset($msj['texto'])) echo $msj['texto']?></div>
                    </div>

                    <section id="seccion_formulario">
                        <div class="formulario">
                            <form action="<?php echo current_url(); ?>" id="frmOlvidoClave" method='post'  onSubmit="return validar_recordar()">
                            <input type="hidden" name="accion" value="olvido_clave" />
                                <div class="form-group">
                                    <label><?php echo lang('registro.correo'); ?></label>
                                    <input type="email" id="txtEmail" name="email" class="form-control" value="<?php if (isset($arDatos['email'])) echo $arDatos['email'];else echo '';?>" required/>
                                </div>
                                <div class="form-group">
                                    <label><?php echo lang('registro.contrasena'); ?></label>
                                    <input type="password" id="txtClave" name="clave" class="form-control"  value="<?php if (isset($arDatos['clave'])) echo $arDatos['clave'];else echo '';?>" required/>
                                    <p class="info"><?php echo lang('registro.contrasena_explicacion'); ?></p>
                                </div>
                                <div class="form-group">
                                    <label><?php echo lang('registro.contrasena2'); ?></label>
                                    <input type="password" id="txtClave2" name="clave2" class="form-control" value="<?php if (isset($arDatos['clave2'])) echo $arDatos['clave2'];else echo '';?>"  required/>
                                </div>                            
                                <div class="form-group">
                                    <?php //echo $this->recaptcha->imprimir(); ?>
                                </div>

                                <div class="text-center"><button type="submit"><?php echo lang('registro.contrasena_nueva'); ?>Crear nueva contraseña</button></div>
                            </form>
                        </div>
                        <div class="footer-nav">
                            <ul>
                                <li><a href="<?php echo site_url('login');?>"><?php echo lang('registro.inicia_sesion'); ?></a></li>
                                <li><a href="<?php echo site_url('login/registro');?>"><?php echo lang('registro.registrate'); ?></a></li>
                            </ul>
                        </div>
                    </section>           
                </div>
            </div>
        </div>
    </section>