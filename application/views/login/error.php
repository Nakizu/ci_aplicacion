 <div class="container">
    <div class="row">
    	<div class="formulario text-center">
		<h4>Todavia no puede registrarse</h4>
		<p>Solo los usuarios invitados se pueden registrar por ahora</p>
		</div>
	</div>

 <div class="footer-nav"><a href="<?php echo site_url('app/login');?>">Si ya estás registrado <?php echo lang('registro.inicia_sesion'); ?></a></div>

</div>