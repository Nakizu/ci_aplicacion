<section id="registro" class="accesosection">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
               	<div class="text-center" id="acceso-header">
					<h2 id="headerlogo"><span>G93</span>Telecomunicaciones</h2>
					<h1><?php echo lang('registro.registrate_gratis'); ?></h1>
					<p>O <a href="<?php echo site_url('login');?>"><?php echo lang('registro.entra'); ?></a> <?php echo lang('registro.tienes_cuenta'); ?></p>
				</div>
    
                 <div id="mensaje_aviso" class="<?php if (empty($msj)) echo 'hidden';?>">
                    <div class="aviso <?php if (isset($msj['class'])) echo $msj['class']?>"><?php if (isset($msj['mensaje'])) echo $msj['mensaje']?></div>
                </div>

            <section id="seccion_formulario">
                <div class="formulario">
                	<form action="<?php echo current_url(); ?>" id="frmNuevoRegistro" method="post"  onSubmit="return validar_registro()">
                    <input type="hidden" name="accion" id="funcion" value="registrar" />
                		<div class="form-group">
                			<label><?php echo lang('registro.correo'); ?></label>
                			<input type="email" id="txtEmail" name="email" class="form-control" required value="<?php if (isset($arDatos['email'])) echo $arDatos['email'];else echo '';?>"/>
                		</div>
                		<div class="row">
                			<div class="col-md-6">
                				<div class="form-group">
									<label><?php echo lang('registro.contrasena'); ?></label>
									<input type="password" id="txtClave" name="clave" class="form-control" value="<?php if (isset($arDatos['clave'])) echo $arDatos['clave'];else echo '';?>"  required/>
									<p class="info"> <?php echo lang('registro.contrasena_explicacion');?></p>
								</div>
                			</div>
                			<div class="col-md-6">
                				<div class="form-group">
									<label><?php echo lang('registro.contrasena2'); ?></label>
									<input type="password" id="txtClave2" name="clave2" class="form-control"  value="<?php if (isset($arDatos['clave2'])) echo $arDatos['clave2'];else echo '';?>"  required/>
								</div>
                			</div>
                		</div>

                        <div class="form-group">
                        <?php //echo $this->recaptcha->imprimir(); ?>
                        </div>

                		<div class="text-center">
                            <button  type="submit"><?php echo lang('registro.registrarse'); ?></button>
                        </div>
                	</form>
                </div>
                <div class="footer-nav"><a href="<?php echo site_url('login');?>"><?php echo lang('registro.inicia_sesion'); ?></a></div>

                </section>
            </div>
        </div>
    </div>
</section>