<?php 
$lang['menu.inicio']='Inicio';
$lang['menu.servicios']='Servicios';
$lang['menu.contactar']='Contactar';
$lang['menu.contactar_titulo']='Contactar';
$lang['menu.contactar_subtitulo']='pongase en contacto con nosotros';
$lang['menu.admin']='Admin';
$lang['menu.servicios']='Servicios';
$lang['menu.imagenes']='Imagenes';
$lang['contactar.titulo_envio']='Envianos un mensaje';

$lang['nombre']='Nombre';
$lang['email']='Email';
$lang['mensaje']='Mensaje';
$lang['enviar']='Enviar';



$lang['registro.inicia_sesion']          = 'Inicia sesión';
$lang['registro.registrate']          = 'regístrate';
$lang['registro.cuenta']          = 'si todavía no tienes una cuenta';

$lang['registro.correo']          = 'Correo electrónico';
$lang['registro.contrasena']          = 'Contraseña';
$lang['registro.contrasena2']          = 'Repetir contraseña';
$lang['registro.iniciar_sesion']          = 'Iniciar Sesión';
$lang['registro.registrate']          = 'Regístrate';
$lang['registro.registrarse']          = 'Regístrarse';

$lang['registro.olvido']          = '¿Has olvidado tu contraseña?';
$lang['registro.registrate_gratis']          = 'Regístrate gratis';

$lang['registro.entra']          = ' inicia sesión ';
$lang['registro.tienes_cuenta']          = 'si ya tienes una cuenta';

$lang['registro.contrasena_explicacion']          = 'Por seguridad solo admitimos claves que tengan por lo menos 6 caracteres sin espacios en blanco.';

$lang['registro.olvido_explicacion']          = 'Introduce tu email y la clave que quieres. Recibirás un email con instrucciones para confirmar el cambio de clave.';

$lang['registro.volver_web']          = 'Volver a la web';
$lang['registro.registrado']          = 'El registro se ha realizado';
$lang['registro.email_existe']          = 'Ese email ya existe';

$lang['registro.no_login']          = 'Los datos no son correctos';

$lang['admin.seccion']          = 'Sección';
$lang['admin.servicio']          = 'Servicio';
$lang['admin.titulo']          = 'Titulo';
$lang['admin.texto']          = 'Texto';
$lang['admin.parrafo1']          = 'Parrafo 1';
$lang['admin.parrafo2']          = 'Parrafo 2';

$lang['guardar']          = 'Guardar';

$lang['portada.visitanos']          = 'Visitanos en: ';

$lang['servicios.titulo']          = 'Servicios ofrecidos';
